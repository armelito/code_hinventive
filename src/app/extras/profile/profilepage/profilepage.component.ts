import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.component.html',
  styleUrls: ['./profilepage.component.scss']
})
export class ProfilepageComponent implements OnInit {
  title = 'Extras V1';

  constructor(
      private router: RouterExtensions,
      private active: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('Profile Page');
  }
  private loadTabRoutes() {
    console.log('ProfilepageComponent - loadTabRoutes', this.active);
    setTimeout(() => {
      this.router.navigate(
        [
          {
            outlets: {
              location: ['location'],
              profile: ['profile'],
              adresse: ['adresse']
            }
          }
        ],
        {
          relativeTo: this.active
        }
      );
    }, 10);
  }
}

