import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '@src/app/extras/auth/auth.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  @Input() value = '';
  public categorieForm: FormGroup;
  categories = ['EXTRA', 'PROPR'];
  selectedCategorieIndex ='0';
  
  constructor(private fb:FormBuilder, private auth: AuthService){ }

  ngOnInit() {
    this.categorieForm = this.fb.group({
      categorieControl: ['EXTRA']
    });
  }
  callCat(event: any) {
    console.log('selected catégorie', event.target.selectedIndex);
    this.auth.Categorie = this.categories[event.target.selectedIndex];
    this.selectedCategorieIndex = event.target.selectedIndex;
  }
}
  
