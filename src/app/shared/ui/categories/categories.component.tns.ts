import { Component, OnInit, Input } from '@angular/core';
import { SelectedIndexChangedEventData, ValueList } from 'nativescript-drop-down';
import { AuthService } from '@src/app/extras/auth/auth.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @Input() value = '';
  name = 'categorie';
  hint = 'categorie';
  selectedCategorieIndex = 0;
  readOnly = true;

  categories = new ValueList<string>([
    { value: 'PROPR', display: 'PROPRIETAIRE' },
    { value: 'GERAN', display: 'GERANT' },
    { value: 'EXTRA', display: 'EXTRA' }
]);
  constructor(private auth: AuthService) { }

  ngOnInit() {
    console.log('categorie value', this.value, this.selectedCategorieIndex);
    if (this.value.length > 0) {
      this.readOnly = true;
      this.selectedCategorieIndex = this.categories.getIndex(this.value);
    }
    console.log('categorie index', this.selectedCategorieIndex, this.categories.getValue(this.selectedCategorieIndex));
  }

dropDownSelectedIndexChanged(args: SelectedIndexChangedEventData) {
  console.log(`Drop Down selected index changed from ${args.oldIndex} to ${args.newIndex}`);
  console.log('categorie', this.categories.getValue(args.newIndex));
  this.auth.Categorie = this.categories.getValue(args.newIndex);
  this.selectedCategorieIndex = args.newIndex;
}
}
