import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionBarComponent } from '@src/app/shared/ui/mission-bar/mission-bar.component';

describe('MissionBarComponent', () => {
  let component: MissionBarComponent;
  let fixture: ComponentFixture<MissionBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
