import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { isAndroid } from 'tns-core-modules/platform';
import { Page, isUserInteractionEnabledProperty } from 'tns-core-modules/ui/page';
import { RouterExtensions } from 'nativescript-angular/router';
import { UIService } from '../ui.service';
import { Subscription } from 'rxjs';
import { AuthService } from '@src/app/extras/auth/auth.service';
import { UserMe } from '@src/app/extras/auth/user.model';

declare var android: any;

@Component({
  selector: 'app-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit, OnChanges, OnDestroy {
    @Input() title: string;
    @Input() showBackButton = true;
    @Input() hasMenu = true;
    hasLogout = false;
    public user: UserMe;
    private usersSub: Subscription;


  constructor(
    private page: Page,
    private router: RouterExtensions,
    private uiService: UIService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    console.log(this.title);
    console.log('hasmenu', this.hasMenu, this.showBackButton);
    this.usersSub = this.authService.getUserUpdateListener().subscribe(
      userData => {
//        console.log('action bar', userData);
        this.user = userData;
      }
    );
//    console.log('isLoggedIn', this.authService.isLoggedIn());
    this.hasLogout = this.authService.isLoggedIn();
//    console.log( 'isLoggedIn', this.authService.isLoggedIn);
}
ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
//  console.log('ngOnChanges - action bar');
//  console.log(this.title);
}
ngOnDestroy() {
  if ( this.usersSub ) {
    this.usersSub.unsubscribe();
  }
}
get android() {
  return isAndroid;
}
get canGoBack() {
  return this.router.canGoBack() && this.showBackButton;
}
onGoBack() {
  this.router.backToPreviousPage();
}
onAuth() {
    this.router.navigateByUrl('/auth');
}
onLogout() {
  this.authService.logout();
}
onLoadedActionBar() {
  console.log('onLoadedActionBar');
  if (isAndroid) {
    console.log('action bar thinks this is android');
    
    const androidToolbar = this.page.actionBar.nativeView;
    const backButton = androidToolbar.getNavigationIcon();
    let color = '#171717';
    if (this.hasMenu) {
      color = '#ffffff';
    }
    if (backButton) {
      backButton.setColorFilter(
        android.graphics.Color.parseColor(color),
        (<any>android.graphics).PorterDuff.Mode.SRC_ATOP
      );
    }
  } else {
    console.log('onLoadedActionBar is ios');
  }
}

onToggleMenu() {
  console.log('onToggleMenu');
  this.uiService.toggleDrawer();
}
}
