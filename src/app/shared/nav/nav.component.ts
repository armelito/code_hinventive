import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '@src/app/helpers/http-service.service';
import { RoutingService } from '@src/app/helpers/routing.service';
import { AuthService } from '@src/app/extras/auth/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
  
})
export class NavComponent implements OnInit {
  titleCompo = 'EXTRA';

  constructor(
    private http: HttpServiceService,
    private router: RoutingService,
    public auth: AuthService) { }
  

  ngOnInit() {
  }

  Extra() {console.log('extra',this.auth.Categorie);
    return this.auth.Categorie === 'EXTRA';
  }

    
  onProfile() {
  
    this.router.replace(['/profile'], true);
}

  onEtablissement() {
    this.router.replace(['/etablissement'])
  }

  onMisList() {
    this.router.replace(['/resto'])
  }

  onMission() {
    this.router.replace(['/mission'])
  }

  onGerant() {
    this.router.replace(['/gerant'])
  }

  onDispo() {
    this.router.replace(['/extra'])
  }

  onMisCreate() {
    this.router.replace(['/'])
  }

  onMisMod() {
    this.router.replace(['/'])
  }

  onMisVal() {
    this.router.replace(['/'])
  }

  onMisDel() {
    this.router.replace(['/'])
  }

  onGerCreate() {
    this.router.replace(['/gerant'])
  }

  
}
